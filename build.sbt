val scalaTest = "org.scalatest" %% "scalatest" % "2.2.4" % "test"
val scalaCheck = "org.scalacheck" %% "scalacheck" % "1.12.3" % "test"

lazy val commonSettings = Seq(
  organization := "newcastle.dojo",
  version := "0.1.0",
  scalaVersion := "2.11.6"
)

lazy val root = (project in file(".")).
  settings(commonSettings: _*).
  settings(scalariformSettings: _*).
  settings(
    name := "Dojo Kata",
    libraryDependencies ++= Seq(scalaTest,scalaCheck)
)