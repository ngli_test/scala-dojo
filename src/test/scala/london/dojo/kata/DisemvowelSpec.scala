package london.dojo.kata

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, FlatSpec}

class DisemvowelSpec extends FlatSpec with Matchers with TableDrivenPropertyChecks{

  val examples = Table(
    ("bob","bb"),
    ("bcd", "bcd"),
    ("ai", ""),
    ("This website is for losers LOL!","Ths wbst s fr lsrs LL!")
  )

  "function" should "remove vowels on all inputs" in {
    forAll(examples) {
      (in,out) => Disemvowel(in) shouldBe out
    }
  }

}
