package london.dojo.kata

import org.scalatest.{Matchers, FlatSpec}

class ShellGameSpec extends FlatSpec with Matchers {

  "game" should "meet the example requirements" in {
    ShellGame.findTheBall(0, Seq((0,1), (2,1), (0,1))) shouldBe 2
  }

}
