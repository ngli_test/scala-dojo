package london.dojo.kata


object Disemvowel {
  val Vowels = Set('a', 'e', 'i', 'o', 'u')
  def apply(inputMessage: String)= inputMessage.filter( !isVowel(_))
  def isVowel(x: Char) = Vowels.contains(x.toLower)
}