package london.dojo.kata

object ShellGame {

  def findTheBall(initialPosition:Int, swaps: Seq[(Int, Int)]) :Int = {
    swaps.foldLeft(initialPosition)(applySwap)
  }

  def applySwap(position: Int, swap:(Int, Int)) = {
    if(position == swap._1) swap._2
    else if(position == swap._2) swap._1
    else position
  }
}
